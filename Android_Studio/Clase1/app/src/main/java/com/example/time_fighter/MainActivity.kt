package com.example.time_fighter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Message
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var welcomeMessageTextView: TextView
    private lateinit var welcomerSubtitleTextView: TextView
    private lateinit var changeTextButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /* Se pasa el id de la vista*/
        welcomeMessageTextView = findViewById(R.id.welcome_message)
        welcomerSubtitleTextView = findViewById(R.id.welcome_subtitle)
        changeTextButton = findViewById(R.id.change_text_button)
        /* Para cambiar el texto cuando se de clic al boton */
        changeTextButton.setOnClickListener { changeMessageAndSubtitle() }
    }

    private fun changeMessageAndSubtitle(){
        welcomeMessageTextView.text = getString(R.string.new_welcome_message)
        welcomerSubtitleTextView.text = getString(R.string.new_welcome_subtitle)
    }
}

package com.example.time_figther

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class GameActivity : AppCompatActivity() {

    private lateinit var activityTitle: TextView
    private lateinit var playerName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        activityTitle = findViewById(R.id.game_title)

        playerName = intent.getStringExtra(MainActivity.INTENT_PLAYER_NAME) ?: "Error"

        activityTitle.text = getString(R.string.get_ready_player, playerName)
    }
}

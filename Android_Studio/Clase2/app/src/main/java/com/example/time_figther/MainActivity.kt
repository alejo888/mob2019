package com.example.time_figther

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var startButton: Button
    private lateinit var playerName: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startButton = findViewById(R.id.start_button)
        playerName = findViewById(R.id.player_name_input)
        // Se conecta al boton con el id del boton
        startButton.setOnClickListener { showGameActivity() }
    }

    private fun showGameActivity(){
        val gameActivityIntent = Intent(this, GameActivity::class.java)

        gameActivityIntent.putExtra(INTENT_PLAYER_NAME, playerName.text.toString())

        // Le pasa el intent que se creo
        startActivity(gameActivityIntent)
    }

    companion object {
        const val INTENT_PLAYER_NAME = "playerName"
    }
}

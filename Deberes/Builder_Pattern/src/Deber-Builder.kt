class Elf {
    var typeElf = ""
    var typeClass = ""
    var alliance = false
    var horde = false

    override fun toString(): String {
        return "Elf(typeElf='$typeElf', typeClass='$typeClass', alliance=$alliance, horde=$horde)"
    }
}

/// builders

abstract class ElfBuilder {
    lateinit var elf : Elf

    fun createNewCharacter(){
        elf = Elf()
    }

    abstract fun buildSkinColor()
    abstract fun buildTypeClass()
    abstract fun buildAlliance()
    abstract fun buildHorde()
}

class nigthElfBuilder : ElfBuilder(){

    override fun buildSkinColor() {
        elf.typeElf = "Night Elf"
    }

    override fun buildTypeClass() {
        elf.typeClass = "Hunter"
    }

    override fun buildAlliance() {
        elf.alliance = true
    }

    override fun buildHorde() {
        elf.horde = false
    }
}

class BloodElfBuilder : ElfBuilder(){

    override fun buildSkinColor() {
        elf.typeElf = "Blood Elf"
    }
    override fun buildTypeClass() {
        elf.typeClass = "Warrior"
    }

    override fun buildAlliance() {
        elf.alliance = false
    }

    override fun buildHorde() {
        elf.horde = true
    }

}

// director

class Azeroth {
    private var elfBuilder : ElfBuilder? = null

    fun setElfBuilder(elfBuilder: ElfBuilder){
        this.elfBuilder = elfBuilder
    }

    fun getElf() : Elf? {
        return elfBuilder!!.elf
    }

    fun constructElf(){
        elfBuilder?.createNewCharacter()
        elfBuilder?.buildSkinColor()
        elfBuilder?.buildTypeClass()
        elfBuilder?.buildAlliance()
        elfBuilder?.buildHorde()
    }
}

fun main(args: Array<String>) {
    val azeroth = Azeroth()
    val bloodElfBuilder = BloodElfBuilder()
    val nigthElfBuilder = nigthElfBuilder()

    azeroth.setElfBuilder(bloodElfBuilder)
    azeroth.constructElf()

    var elf = azeroth.getElf()
    println(elf)

    azeroth.setElfBuilder(nigthElfBuilder)
    azeroth.constructElf()

    elf = azeroth.getElf()
    println(elf)
}
interface Device {
    val type: String
    fun switchOn(): String
}

interface Vendor {
    val brand: String
    /**
     * Vendor supports the provided [device]
     */
    fun support(device: Device): String
}

class PhoneDevice(val vendor: Vendor, override val type: String = "Phone") : Device {
    override fun switchOn(): String = vendor.support(this)
}

class TabletDevice(val vendor: Vendor, override val type: String = "Tablet") : Device {
    override fun switchOn(): String = vendor.support(this)
}

class XiaomiVendor(override val brand: String = "Xiaomi") : Vendor {
    override fun support(device: Device): String = "$brand supports ${device.type}"
}

class NokiaVendor(override val brand: String = "Nokia") : Vendor {
    override fun support(device: Device): String = "$brand supports ${device.type}"
}